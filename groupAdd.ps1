# Start transcript
Start-Transcript -Path C:\PathToLog\UserToGroup.log -Append

# Импорт AD Module
Import-Module ActiveDirectory

# Импорт данных со списком пользователей из файла CSV
$Users = Import-Csv "C:\PathToCSV\Users.csv"

# Вводим имя групп
$Group1 = "Group_name_1"
$Group2 = "Group_name_2"

foreach ($User in $Users) {
  # Получаем значение поля Name
  $Name = $User.Name
  
  # Получаем значение поля SamAccountName связанное со значением Name
  $ADUser = Get-ADUser -Filter "Name -eq '$Name'" | Select-Object SamAccountName
  
  # Проверяем, есть ли пользователь из CSV в AD
  if ($ADUser -eq $null) {
	  Write-Host "$Name does not exist in AD" -ForegroundColor Red
  }
  else {
      # Получаем членство в группе пользователей AD
	  $ExistingGroups = Get-ADUser -Properties MemberOf $ADUser.SamAccountName | Select-Object Name
  
      # Проверяем, есть ли пользователь уже в группе
	  if ($ExistingGroups.Name -eq $Group1) {
		  Write-Host "$Name already exists in $group" -ForegroundColor Yellow
	  }
	  else {
		  # Добавим пользователя в группу
                  echo $ADUser.SamAccountName
		  Add-ADGroupMember -Identity $Group1 -Members $ADUser.SamAccountName
                  
                  Write-Host "Added $Name to $Group" -ForeGroundColor Green
	  }
  }
}

# Скопируем пользователей из Group1 в Group2
Get-ADGroupMember -Identity $Group1 | ForEach-Object { Add-ADGroupMember -Identity $Group2 -Members $_ }

Stop-Transcript
